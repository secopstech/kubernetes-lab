
# Deploying kubernetes cluster via kubespray

Kubespray is a project that contains a bunch of ansible playbooks which deploy kubernetes clusters in an automated way. It is very suitable
installation, configuration and maintaining method for on-premise environments since you can use it to deploy kubernetes clusters on different
linux distros like, ubuntu, coreos and centos which in this document we're going to explain how to install, configure and maintain a CentOS7 based
kubernetes cluster.

In order to deploy a redundant k8s cluster you need to create at least 3 nodes. With this scenerio all main k8s components (master,node and etcd) will be
installed on these 3 nodes, but if you have enough compute resource you can deploy the cluster's main components on seperated hosts, in this scenerio you should
ideally have at least 9 hosts (3 hosts for masters, 3 for nodes and 3 for etcd cluster.)

If your kubernetes cluster will be used as a production environment then deploy the main components on seperated hosts.  Also, as we now kubespray uses ansible,
there should be a host that holds the playbooks and configurations which responsible for running playbooks against to k8s-nodes over SSH to deploy the cluster.
This host can be a seperated host or you can use one of the k8s-nodes for this purpose.
In our example we'll use three hosts named **k8s-host-0{1,2,3}** and ansible host will be **k8s-host-01**

There are two main preperation part of this document:
- Ansible host preperations
- k8s nodes preperations.

After preperation steps we'll initiate kubespray cluster.yml playbook to deploy our first cluster.

So let's get started with ansible preperation tasks.

## Preparing ansible

As we've mentioned above our ansible host will be **k8s-host-01** and in this section all necessary action steps will be done in this host.

First of all disable selinux and firewalld:

```
# setenforce 0 && \
  sed -i "s/^SELINUX\=enforcing/SELINUX\=disabled/g" /etc/selinux/config && \
  systemctl disable firewalld; systemctl stop firewalld; systemctl mask firewalld
```

Then install required packages by kubespray:

```
# yum install -y epel-release && \
  yum install -y git python-pip python-devel openssl-devel gcc libffi-devel wget && \
  pip install --upgrade pip
```


Now we'll create an user named kubespray and add to sudo (as NOPASSWD). Actually this is not a "must have" situation but I think isolating the kubespray
from the root useris a good practise.

```
# adduser kubespray && \
  echo 'kubespray    ALL=(ALL)       NOPASSWD: ALL' > /etc/sudoers.d/kubespray
```

Switch to kubespray and create an SSH key pair. We'll use this keypair to establish passwordless connections to k8s nodes.

```
# su - kubespray
$ ssh-keygen -b 4096 -t rsa -f ~/.ssh/id_rsa
```

Get the latest kubespray release from https://github.com/kubernetes-incubator/kubespray/releases and extract it.
(As the date of this document written the latest version is 2.5.0)

```
$ wget https://github.com/kubernetes-incubator/kubespray/archive/v2.5.0.tar.gz && \
  tar xvfz v2.5.0.tar.gz && \
  cd kubespray-2.5.0
```

Now we install required python packages by using pip

```
$ sudo pip install -r requirements.txt
```

Next steps are related to cluster configurations and there are some variables which shoud be defined correctly in you environment.

By default, when you initiate a cluster deployment via kubespray, it sets the cluster name as **cluster.local**, we'll change it via environment variables.
So please change $CLUSTER_NAME and $CLUSTER_DOMAIN variables as your needs.

```
$ CLUSTER_NAME="cluster-name"
$ CLUSTER_DOMAIN="domain"
$ CLUSTER_FQDN="$CLUSTER_NAME.$CLUSTER_DOMAIN"
```

Create a directory that presents your cluster name. This directory will contains cluster specific ansible inventory and variable files and we get those files by
copying them from the sample directory.
```
$ mkdir inventory/$CLUSTER_NAME
$ cp -rfp inventory/sample/* inventory/$CLUSTER_NAME/
```

Now our ansible environment is ready to be configured as our specifis needs.
There are two main configuration files named **all.yml** and **k8s-cluster.yml** and both files is placed in inventory/$CLUSTER_NAME/group_vars/ directory.

Let's change the cluster name first.
```
$ sed -i "s/cluster_name\: cluster.local/cluster_name\: $CLUSTER_FQDN/g" inventory/$CLUSTER_NAME/group_vars/k8s-cluster.yml
```

And set the auth modes which we'll disable anonymous auth and enable basic and token auth.:
```
$ sed -i "s/kube_api_anonymous_auth\: true/kube_api_anonymous_auth\: false/g" inventory/$CLUSTER_NAME/group_vars/k8s-cluster.yml
$ sed -i "s/#kube_basic_auth\: false/kube_basic_auth: true/g" inventory/$CLUSTER_NAME/group_vars/k8s-cluster.yml
$ sed -i "s/#kube_token_auth\: false/kube_token_auth: true/g" inventory/$CLUSTER_NAME/group_vars/k8s-cluster.yml
```

Also enable kubernetes network policy support:
```
$ sed -i "s/enable_network_policy\: false/enable_network_policy\: true/g" inventory/$CLUSTER_NAME/group_vars/k8s-cluster.yml
```

I prefer to use weave as CNI (for further reading, here is a good comparison https://chrislovecnm.com/kubernetes/cni/choosing-a-cni-provider/)
so we set the kube_network_plugin variable as weave and define a password for encryption.
```
$ sed -i "s/kube_network_plugin\: calico/kube_network_plugin\: weave/g" inventory/$CLUSTER_NAME/group_vars/k8s-cluster.yml
$ sed -i "s/weave_password\: EnterPasswordHere/weave_password\: $(date | md5sum | cut -d' ' -f1)/g" inventory/$CLUSTER_NAME/group_vars/k8s-cluster.yml
```

Next, we need to define ansible_pkg_mgr variable in bootstrap-centos.yml
```
$ sed -i "/name: Install packages requirements for bootstrap/a \ \ vars\:\n \ \  ansible_pkg_mgr\: yum" roles/bootstrap-os/tasks/bootstrap-centos.yml
```

now set the bootstrap_os variable to as centos in the all.yml file as we use CentOS 7 distribution.
```
$ sed -i "s/bootstrap_os\: none/bootstrap_os\: centos/g" inventory/$CLUSTER_NAME/group_vars/all.yml
```

also kube_read_only_port variable should be enabled in order to heapster and metrics-server work.
```
$ sed -i "s/#kube_read_only_port\: 10255/kube_read_only_port\: 10255/g" inventory/$CLUSTER_NAME/group_vars/all.yml
```

rename the hosts.ini file as inventory.cfg
```
$ mv inventory/$CLUSTER_NAME/hosts.ini inventory/$CLUSTER_NAME/inventory.cfg
```

inventory.cfg file is main configuration file that defines the k8s host roles. So you need to be set as properly. In our example we have three nodes
named k8s-host-0{1,2,3} so our inventory.cfg should look like below:

**Note:** You need to also set the bastion and ip variable options if your network requiers it. 
And there is an another section related to nginx ingress controller. But we'll deploy ingress controller seperately (in a more customized way) 
we don't use the ingress section of inventory so simply delete it.

```
[kube-master]
node1
node2

[etcd]
node1
node2
node3

[kube-node]
node2
node3
node4
node5
node6

[k8s-cluster:children]
kube-node
kube-master
```

#### A few notes about connectivity:

- Your nodes should be resolve each other hostname. So create proper DNS records or add them to the hosts file.
- If your nodes serve ssh other than 22, set the correct port in your ansible.cfg file.

Ansible host preperation is done and now we will configure our k8s nodes.

Please note that we have used k8s-host-01 as ansible host in our example. So some of the k8s-node preperation setps are already done at that node.

## Preparing k8s nodes

Disable selinux and firewalld.
```
# setenforce 0
# sed -i "s/^SELINUX\=enforcing/SELINUX\=disabled/g" /etc/selinux/config
# systemctl disable firewalld; sudo systemctl stop firewalld; sudo systemctl mask firewalld
```

Swap should be disabled on all nodes orherwise kubespray will fail. So let's disable it.
```
# swapoff -a

```
**Note:** Don't forget to remove the swap entry in /etc/fstab.

And add kubespray user and setup sudoers for it.
```
# adduser kubespray
# sed -i '/visiblepw/i Defaults   \!requiretty' /etc/sudoers
# echo 'kubespray    ALL=(ALL)       NOPASSWD: ALL' > /etc/sudoers.d/kubespray
```

Now you need to distribute the ansible host's (k8s-host-01) pubkey to the k8s nodes' kubespray user as authorized key. Yes, you need to also put the pubkey to
host-01's authorized_keys file, since ansible will make a ssh connection for itself too by using its primary ip address.

## Deploy kubernetes cluster from the ansible instance

OK, we are ready to deploy cluster. It's a very simple step, only run cluster.yml playbook as follows:
```
# su - kubespray
$ cd kubespray-2.5.0
$ CLUSTER_NAME="cluster_name"
$ ansible-playbook -u $(whoami) -b -i inventory/$CLUSTER_NAME/inventory.cfg cluster.yml
```

After deployment process the cluster should be installed properly.

### Accessing kubernetes API from k8s-node with kubectl

By default kubernetes API can be accessible via kubectl from any k8s host.

Let's check it on host-01:

```
# kubectl get no
NAME          STATUS    ROLES         AGE       VERSION
k8s-host-01   Ready     master,node   1d        v1.9.5
k8s-host-02   Ready     master,node   1d        v1.9.5
k8s-host-03   Ready     master,node   1d        v1.9.5
```

As you can see our k8s cluster is ready and all nodes act as master and node (also etcd host).

### Accessing kubernetes API from a remote kubectl

When you initiate cluster deployment, kubespray adds an admin user named "kube" by default. This user's password is created in runtime and placed
in inventory/$CLUSTER_NAME/credentials/kube_user.creds file. Although we'll create a user and join it a new admin group in order to do more granular access control
(also this will interact to kubernetes API by using its PKI key pair instead of basic auth.) at the next section, if you still want to use the default
user to connect kubernetes API you can follow the steps below:

Create a kubeconfig file on your local machine.

**Note:**
This machine should meets the requirements listed below:
- Able to access to kubernetes API on https://kubernetes.default.svc.$CLUSTER_FQDN:6443 , so:
 - Masters tcp port 6443 needs to be accessable from your client.
 - $CLUSTER_FQDN resolves to one of the master's primary IP addresses (you can load balance them).

And paste:
```
apiVersion: v1
kind: Config
current-context: admin-$CLUSTER_FQDN
preferences: {}
contexts:
- context:
    cluster: $CLUSTER_FQDN
    user: admin
  name: admin.$CLUSTER_FQDN
clusters:
- cluster:
    certificate-authority-data: $CLUSTER_CA_PEM_BASE64_IN_ONE_LINE
    server: https://kubernetes.default.svc.$CLUSTER_FQDN:6443
  name: $CLUSTER_FQDN
users:
- name: admin
  user:
    username: kube
    password: $PASSWORD
```

**$CLUSTER_FQDN** should be changed as your cluster.
Also, **certificate-authority-data** have to contains /etc/kubernetes/ssl/ca.pem information as one line and base64 encoded format.
To grab the /etc/kubernetes/ssl/ca.pem required format, you can run something like this in one of your master node:
`# cat /etc/kubernetes/ssl/ca.pem |base64 -w 0`
Copy the output and paste it to CLUSTER_CA_PEM_BASE64_IN_ONE_LINE section.

Also, password section have to be changed with the password that stored in inventory/$CLUSTER_NAME/credentials/kube_user.creds file.

If you prepare the kubeconfig properly, you can set this file as kubectl config via KUBECONFIG variable:

```
# export KUBECONFIG=/path/to/kubeconfig
```

Now test your connectivity:

```
# kubectl get no -o wide
NAME          STATUS    ROLES         AGE       VERSION   EXTERNAL-IP   OS-IMAGE                KERNEL-VERSION              CONTAINER-RUNTIME
k8s-host-01   Ready     master,node   1d        v1.9.5    <none>        CentOS Linux 7 (Core)   3.10.0-862.6.3.el7.x86_64   docker://17.3.2
k8s-host-02   Ready     master,node   1d        v1.9.5    <none>        CentOS Linux 7 (Core)   3.10.0-862.6.3.el7.x86_64   docker://17.3.2
k8s-host-03   Ready     master,node   1d        v1.9.5    <none>        CentOS Linux 7 (Core)   3.10.0-862.6.3.el7.x86_64   docker://17.3.2
```


# Configutations

In this document we'll cover the configuration steps mentioned below:

- Create an admin group and and user
- Enable dashboard
- Deploy nginx ingress controller
- Enable metrics server

## Create an user with administrator privileges

This section consists of four phases:
1. Create a "ClusterRole" for the custom admin group
2. Create a "ClusterRoleBinding" for our newly created "ClusterRole"
3. Create an user certificate for kubectl
4. kubeconfig configuration

### 1. Create a ClusterRole

First of all we're going to create a cluster wide role with full permission to any api group. In our example the ClusterRole name is **secopstech-admins-cr**.
So each binding member to this ClusterRole, can be able to access any kubernetes API group. So change it as your needs.

On on of your master hosts, run:
```
# cat <<EOF | kubectl apply -f -
kind: ClusterRole
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: secopstech-admins-cr
rules:
- apiGroups:
  - '*'
  resources:
  - '*'
  verbs:
  - '*'
- nonResourceURLs:
  - '*'
  verbs:
  - '*'
EOF
```


### 2. Create a ClusterRoleBinding

Next, we create a "ClusterRoleBinding" to give access "ClusterRole" named secopstech-admins-cr for members of **secopstech-admin** group. You need to change the
**ClusterRoleBinding** name, **group name** as your needs, also define your **ClusterRole** name which you've defined the last section.
(In our example it was secopstech-admins-cr)
```
# cat <<EOF | kubectl apply -f -
kind: ClusterRoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: secopstech-admins-crb
subjects:
- kind: Group
  name: secopstech-admins
  apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: ClusterRole
  name: secopstech-admins-cr
  apiGroup: rbac.authorization.k8s.io
EOF
```

This creates a *ClusterRoleBinding* named *secopstech-admins-crb*, binds it to *secopstech-admins-cr ClusterRole* and permits ClusterRole's privileges 
(which is permitted for all API group calls) for each users who provide a certificate that issued by this kubernetes cluster CA. Also as we'll see the 
next section, any user certificate organization (O) should be defined as the group name to access the cluster with admin privileges.

### 3. Crate an user certificate
When you create a user certificate, there are two important variable that you need to define before:
- $USERNAME = This is the username to be created
- $GROUP_NAME = This is the group name which we'll be bind our admin "ClusterRole" that we've created the last section. Our example is "secopstech-admins"

So let's define them first as environment variables:

```
# USERNAME="my-admin-user"
# GROUP_NAME="secopstech-admins"
```

Now we ready to issue for our user by using our cluster's CA.
We'll run our commands as root and create a hidden folder in root's home directory to place the generated certificates which should be keept as secure as possible.


```
# mkdir ~/.k8s-user-certs
# openssl genrsa -out ~/.k8s-user-certs/$USERNAME.key 2048
# openssl req -new \
    -key ~/.k8s-user-certs/$USERNAME.key \
    -out ~/.k8s-user-certs/$USERNAME.csr \
    -subj "/CN=$USERNAME/O=$GROUP_NAME"
# openssl x509 -req \
    -in ~/.k8s-user-certs/$USERNAME.csr \
    -CA /etc/kubernetes/ssl/ca.pem \
    -CAkey /etc/kubernetes/ssl/ca-key.pem \
    -CAcreateserial \
    -out ~/.k8s-user-certs/$USERNAME.crt \
    -days 365
```

Our user certificate should be issued, let's check it:
```
ls -l .k8s-user-certs/
total 12
-rw-r--r-- 1 root root 1013 Jul  8 21:29 my-admin-user.crt
-rw-r--r-- 1 root root  932 Jul  8 21:29 my-admin-user.csr
-rw-r--r-- 1 root root 1679 Jul  8 21:28 my-admin-user.key
```

Now we can configure our kubeconfig by using these PKI files.

### 4. kubeconfig Configuration

Create a kubeconfig file on your local machine and populate it as follows and change 

```
apiVersion: v1
kind: Config
current-context: admin.$CLUSTER_FQDN
preferences: {}
contexts:
- context:
    cluster: $CLUSTER_FQDN
    user: $USERNAME
  name: admin.$CLUSTER_FQDN
clusters:
- cluster:
    certificate-authority-data: $CLUSTER_CA_PEM_BASE64_IN_ONE_LINE
    server: https://kubernetes.default.svc.$CLUSTER_FQDN:6443
  name: $CLUSTER_FQDN
users:
- name: $USERNAME
  user:
    client-certificate-data: $USERNAME.CRT_CONTENT
    client-key-data: $USERNAME.KEY_CONTENT
```
In order to set kubeconfig properly these variables should be defined:

- **$CLUSTER_CA_PEM_CONTENT:** This is your kubernetes cluster CA public key file which places in /etc/kubernetes/ssl/ca.pem on any master hosts.
It have to be presented in one line base64 encoded format in kubeconfig. To grab it in correct format run: `# cat /etc/kubernetes/ssl/ca.pem |base64 -w 0`
and copy the output to paste it as *certificate-authority-data*
 
- **CLUSTER_FQDN:** This is our clusters name variable which we've defined in installation section.

- **USERNAME:** In our example this is *my-admin-user*

- **USERNAME.CRT_CONTENT:** This is our user's certificate content and its should be as one line base64 encoded format too. You can convert it as follow:
    `# cat ~/.k8s-user-certs/my-admin-user.crt |base64 -w 0` and use it as *client-certificate-data*

- **USERNAME.KEY_CONTENT:** This is our user's private key content and also its must be as one line base64 encoded format:
    `# cat ~/.k8s-user-certs/my-admin-user.key |base64 -w 0` copy and paste it as *client-key-data*

If you populate the kubeconfig you can set this file as kubectl config via KUBECONFIG variable:

```
# export KUBECONFIG=/path/to/kubeconfig
```

Now test your connectivity:

```
# kubectl get no -o wide
NAME          STATUS    ROLES         AGE       VERSION   EXTERNAL-IP   OS-IMAGE                KERNEL-VERSION              CONTAINER-RUNTIME
k8s-host-01   Ready     master,node   1d        v1.9.5    <none>        CentOS Linux 7 (Core)   3.10.0-862.6.3.el7.x86_64   docker://17.3.2
k8s-host-02   Ready     master,node   1d        v1.9.5    <none>        CentOS Linux 7 (Core)   3.10.0-862.6.3.el7.x86_64   docker://17.3.2
k8s-host-03   Ready     master,node   1d        v1.9.5    <none>        CentOS Linux 7 (Core)   3.10.0-862.6.3.el7.x86_64   docker://17.3.2
```

## Kubernetes Dashboard Settings

Kubespray installs the dashboard by default (since k8s 1.7) and you can authenticate by using the default kube admin user.
However we'll create an service account with admin privileges and use its token to access the dashboard.

Create an Service account named dashboard-admin:

```
cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: ServiceAccount
metadata:
  name: dashboard-admin
  namespace: kube-system
EOF
```

Then create an ClusterRoleBinding for the dashboard-admin ServiceAccount and assign it to cluster-admin ClusterRole.

```
cat <<EOF | kubectl apply -f -
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: dashboard-admin
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: dashboard-admin
  namespace: kube-system
EOF
```

Now you can extract the bearer token from the dashboard-admin ServiceAccount to use it on the dashboard authentication.

```
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep dashboard-admin | awk '{print $1}') |egrep "token:" |awk '{print $2}'
```

Use kubectl proxy to access the dashboard:

```
kubectl proxy

http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/#!/login
```

And you can use your token to access the dashboard.

## Deploy nginx ingress controller

In order to use k8s ingress feature in on-premise environments, you need to deploy an ingress controller,
currently there are many ingress controller solution for on-premises like nginx, haproxy, traefik etc.
We'll use nginx load balancer as ingress controller to expose our services to outside of the k8s cluster.
For further information check [the official documentation](https://github.com/kubernetes/ingress-nginx)


Clone nginx ingress controller repo from https://bitbucket.org/secopstech/nginx-ingress-controller/src/master/

```
git clone git@bitbucket.org:secopstech/nginx-ingress-controller.git
```
And deploy it:

```
kubectl apply -f nginx-ingress-controller/configs/
```

configs has an ingress configuration for nginx_status so you can access the controller status via https://status.domain.com/nginx_status
url. Note that you need to point status.domain.com to your k8s nodes by a DNS record or hosts file entry. 

Also there is an example web app in test-app repo. This will create two deployment for api.domain.com
and web.domain.com and expose them via nginx ingress controller, so you can access them https://{api,web}.domain.com
in order to check ingress controller functionality. Note that you need to set these domains should revolves your k8s
nodes IP addresses.

```
kubectl apply -f nginx-ingress-controller/test-app
```


## Deploy heapster for kubectl top
Although heapster is deprecated as of Kubernetes 1.11, kubectl still needs it to run its *top* command. 
So we need to deploy it:

```
git clone https://bitbucket.org/secopstech/heapster
kubectl apply -f heapster/heapster.yaml
```

## Deploy metrics-server

Starting from Kubernetes 1.8, collecting resource usage metrics such as container CPU and memory usage is provided by the Metrics API
and [Metrics Server](https://github.com/kubernetes-incubator/metrics-server) is the default cluster-wide aggregator 
for this purpose. For more details about Metrics API check https://kubernetes.io/docs/tasks/debug-application-cluster/core-metrics-pipeline/

kubespray enables the aggregation layer by default so we need to only deploy the metrics-server with the following steps:

```
git clone https://github.com/kubernetes-incubator/metrics-server.git
cd metrics-server
kubectl apply -f deploy/1.8+/
``` 

Now, v1beta1.metrics.k8s.io apiservice should be deployed.

Check it:

```
kubectl get apiservices |egrep metric
```

and you can call it via kubectl. For example this should return
basic node metrics.

```
kubectl get --raw "/apis/metrics.k8s.io/v1beta1/nodes" |jq .
```

The output should look like below:

```

  "kind": "NodeMetricsList",
  "apiVersion": "metrics.k8s.io/v1beta1",
  "metadata": {
    "selfLink": "/apis/metrics.k8s.io/v1beta1/nodes"
  },
  "items": [
    {
      "metadata": {
        "name": "kube-host-02",
        "selfLink": "/apis/metrics.k8s.io/v1beta1/nodes/kube-host-02",
        "creationTimestamp": "2018-07-09T11:42:55Z"
      },
      "timestamp": "2018-07-09T11:42:00Z",
      "window": "1m0s",
      "usage": {
        "cpu": "247m",
        "memory": "1304292Ki"
      }
    },
    {
      "metadata": {
        "name": "kube-host-03",
        "selfLink": "/apis/metrics.k8s.io/v1beta1/nodes/kube-host-03",
        "creationTimestamp": "2018-07-09T11:42:55Z"
      },
      "timestamp": "2018-07-09T11:42:00Z",
      "window": "1m0s",
      "usage": {
        "cpu": "462m",
        "memory": "1246944Ki"
      }
    },
    {
      "metadata": {
        "name": "kube-host-01",
        "selfLink": "/apis/metrics.k8s.io/v1beta1/nodes/kube-host-01",
        "creationTimestamp": "2018-07-09T11:42:55Z"
      },
      "timestamp": "2018-07-09T11:42:00Z",
      "window": "1m0s",
      "usage": {
        "cpu": "254m",
        "memory": "1464520Ki"
      }
    }
  ]
}
```

## Extending cluster

```
 ansible-playbook -u $(whoami) -b -i ~/kubespray/inventory/inventory.cfg ~/kubespray/cluster.yml --limit $NEW_NODE
```
