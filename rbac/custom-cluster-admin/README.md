# RBAC for admin users

After creating CR and CRB objects via kubectl, issue an user cert by using k8s master CA like below:

```
# openssl genrsa -out ~/.k8s-user-certs/$USERNAME.key 2048
# openssl req -new \
    -key ~/.k8s-user-certs/$USERNAME.key \
    -out ~/.k8s-user-certs/$USERNAME.csr \
    -subj "/CN=$USERNAME/O=$GROUP_NAME"
# openssl x509 -req \
    -in ~/.k8s-user-certs/$USERNAME.csr \
    -CA /etc/kubernetes/ssl/ca.pem \
    -CAkey /etc/kubernetes/ssl/ca-key.pem \
    -CAcreateserial \
    -out ~/.k8s-user-certs/$USERNAME.crt \
    -days 365
```

- CN represents the user
- O represents the group which defined in ClusterRoleBinding

Now you can use your cert and key in your kubeconfig file to access the cluster as admin.
